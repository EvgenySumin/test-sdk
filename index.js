'use strict'

var util = require('util')
var fs = require('fs')
var sdk = require('./src/index.js')

var API_URL = {
  local : 'https://localhost:3000',
  production: 'https://api.rtsmunity.com:6210',
  sandbox: 'https://api.sandbox.rtsmunity.com:6210'
}

// Extended API client
function ApiClient (apiKey, clientKeyPath, clientCertPath, passphrase, mode) {
  sdk.ApiClient.call(this)

  this.basePath = API_URL[mode] || API_URL['local']
  this.defaultHeaders = {
    'X-Api-Key': apiKey
  }
  this._key = fs.readFileSync(clientKeyPath, 'utf-8')
  this._cert = fs.readFileSync(clientCertPath, 'utf-8')
  this._passphrase = passphrase
}

ApiClient.prototype.applyAuthToRequest = function(request, authNames) {
  ApiClient.super_.prototype.applyAuthToRequest.call(this, request, authNames)
  request.key(this._key)
  request.cert(this._cert)
  request.passphrase(this._passphrase)
}

ApiClient.prototype.deserialize = function(response, returnType) {
  console.log('-----')
  console.log(response && response.body)
  console.log('=====')

  return ApiClient.super_.prototype.deserialize.call(this, response, returnType)
};

util.inherits(ApiClient, sdk.ApiClient)

// Export patched classes
module.exports = function(apiKey, clientKeyPath, clientCertPath, passphrase, mode) {
  var apiClient = new ApiClient(apiKey, clientKeyPath, clientCertPath, passphrase, mode)

  var resultSdk = {}
  for (var i in sdk) {
    if (i === 'ApiClient') { continue; } // Skip ApiClient

    if (sdk.hasOwnProperty(i) && (typeof(sdk[i]) === 'function')) {
      resultSdk[i] = sdk[i].bind(null, apiClient)
    }
  }

  return resultSdk
}

/**
 * PULL API
 * REST api for static sport data.  Every resource is protected by an **API key** which must be placed in X-Api-Key reques header:          X-Api-Key: your_api_key  and every request has to be made with client certificate.  
 *
 * OpenAPI spec version: 
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'api/LeaguesApi', 'api/LiveApi', 'api/MarketApi', 'api/MarketsApi', 'api/MatchApi', 'api/MatchesApi', 'api/PlayersApi', 'api/SeriesApi', 'api/SportsApi', 'api/TeamsApi', 'api/TournamentsApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./api/LeaguesApi'), require('./api/LiveApi'), require('./api/MarketApi'), require('./api/MarketsApi'), require('./api/MatchApi'), require('./api/MatchesApi'), require('./api/PlayersApi'), require('./api/SeriesApi'), require('./api/SportsApi'), require('./api/TeamsApi'), require('./api/TournamentsApi'));
  }
}(function(ApiClient, LeaguesApi, LiveApi, MarketApi, MarketsApi, MatchApi, MatchesApi, PlayersApi, SeriesApi, SportsApi, TeamsApi, TournamentsApi) {
  'use strict';

  /**
   * REST_api_for_static_sport_data_Every_resource_is_protected_by_an_API_key_which_must_be_placed_in_X_Api_Key_reques_header________X_Api_Key_your_api_keyand_every_request_has_to_be_made_with_client_certificate_.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var PullApi = require('index'); // See note below*.
   * var xxxSvc = new PullApi.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new PullApi.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
   * and put the application logic within the callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new PullApi.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new PullApi.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 1.0.0
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The LeaguesApi service constructor.
     * @property {module:api/LeaguesApi}
     */
    LeaguesApi: LeaguesApi,
    /**
     * The LiveApi service constructor.
     * @property {module:api/LiveApi}
     */
    LiveApi: LiveApi,
    /**
     * The MarketApi service constructor.
     * @property {module:api/MarketApi}
     */
    MarketApi: MarketApi,
    /**
     * The MarketsApi service constructor.
     * @property {module:api/MarketsApi}
     */
    MarketsApi: MarketsApi,
    /**
     * The MatchApi service constructor.
     * @property {module:api/MatchApi}
     */
    MatchApi: MatchApi,
    /**
     * The MatchesApi service constructor.
     * @property {module:api/MatchesApi}
     */
    MatchesApi: MatchesApi,
    /**
     * The PlayersApi service constructor.
     * @property {module:api/PlayersApi}
     */
    PlayersApi: PlayersApi,
    /**
     * The SeriesApi service constructor.
     * @property {module:api/SeriesApi}
     */
    SeriesApi: SeriesApi,
    /**
     * The SportsApi service constructor.
     * @property {module:api/SportsApi}
     */
    SportsApi: SportsApi,
    /**
     * The TeamsApi service constructor.
     * @property {module:api/TeamsApi}
     */
    TeamsApi: TeamsApi,
    /**
     * The TournamentsApi service constructor.
     * @property {module:api/TournamentsApi}
     */
    TournamentsApi: TournamentsApi
  };

  return exports;
}));

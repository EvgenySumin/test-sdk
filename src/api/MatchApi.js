/**
 * PULL API
 * REST api for static sport data.  Every resource is protected by an **API key** which must be placed in X-Api-Key reques header:          X-Api-Key: your_api_key  and every request has to be made with client certificate.  
 *
 * OpenAPI spec version: 
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.PullApi) {
      root.PullApi = {};
    }
    root.PullApi.MatchApi = factory(root.PullApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';

  /**
   * Match service.
   * @module api/MatchApi
   * @version 1.0.0
   */

  /**
   * Constructs a new MatchApi. 
   * @alias module:api/MatchApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;


    /**
     * Callback function to receive the result of the sportMatchPositionsGet operation.
     * @callback module:api/MatchApi~sportMatchPositionsGetCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * List All Match positions
     * 
     * @param {String} sport Sport key
     * @param {module:api/MatchApi~sportMatchPositionsGetCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.sportMatchPositionsGet = function(sport, callback) {
      var postBody = null;

      // verify the required parameter 'sport' is set
      if (sport == undefined || sport == null) {
        throw "Missing the required parameter 'sport' when calling sportMatchPositionsGet";
      }


      var pathParams = {
        'sport': sport
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json; charset=utf-8'];
      var returnType = null;

      return this.apiClient.callApi(
        '/{sport}/match/positions', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

    /**
     * Callback function to receive the result of the sportMatchStatesGet operation.
     * @callback module:api/MatchApi~sportMatchStatesGetCallback
     * @param {String} error Error message, if any.
     * @param data This operation does not return a value.
     * @param {String} response The complete HTTP response.
     */

    /**
     * List All Matches states
     * 
     * @param {String} sport Sport key
     * @param {module:api/MatchApi~sportMatchStatesGetCallback} callback The callback function, accepting three arguments: error, data, response
     */
    this.sportMatchStatesGet = function(sport, callback) {
      var postBody = null;

      // verify the required parameter 'sport' is set
      if (sport == undefined || sport == null) {
        throw "Missing the required parameter 'sport' when calling sportMatchStatesGet";
      }


      var pathParams = {
        'sport': sport
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = [];
      var contentTypes = [];
      var accepts = ['application/json; charset=utf-8'];
      var returnType = null;

      return this.apiClient.callApi(
        '/{sport}/match/states', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }
  };

  return exports;
}));

# PullApi.SeriesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportSeriesGet**](SeriesApi.md#sportSeriesGet) | **GET** /{sport}/series | List All Series for tournament
[**sportSeriesStatesGet**](SeriesApi.md#sportSeriesStatesGet) | **GET** /{sport}/series/states | List All Series states
[**sportSeriesStatesStateIdGet**](SeriesApi.md#sportSeriesStatesStateIdGet) | **GET** /{sport}/series/states/{state_id} | Get concrete Series state
[**sportSeriesTypesGet**](SeriesApi.md#sportSeriesTypesGet) | **GET** /{sport}/series/types | List All Series types


<a name="sportSeriesGet"></a>
# **sportSeriesGet**
> sportSeriesGet(sport, opts)

List All Series for tournament



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.SeriesApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'tournamentId': 3.4 // Number | ID of the tournament in the form of an integer
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportSeriesGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **tournamentId** | **Number**| ID of the tournament in the form of an integer | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportSeriesStatesGet"></a>
# **sportSeriesStatesGet**
> sportSeriesStatesGet(sport)

List All Series states



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.SeriesApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportSeriesStatesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportSeriesStatesStateIdGet"></a>
# **sportSeriesStatesStateIdGet**
> sportSeriesStatesStateIdGet(stateId, sport)

Get concrete Series state



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.SeriesApi();

var stateId = 3.4; // Number | ID of the state in the form of an integer

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportSeriesStatesStateIdGet(stateId, sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **stateId** | **Number**| ID of the state in the form of an integer | 
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportSeriesTypesGet"></a>
# **sportSeriesTypesGet**
> sportSeriesTypesGet(sport)

List All Series types



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.SeriesApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportSeriesTypesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


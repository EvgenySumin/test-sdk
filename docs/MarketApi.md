# PullApi.MarketApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportMarketCloseTimesGet**](MarketApi.md#sportMarketCloseTimesGet) | **GET** /{sport}/market/close_times | List All Markets close times
[**sportMarketResolveTimesGet**](MarketApi.md#sportMarketResolveTimesGet) | **GET** /{sport}/market/resolve_times | List All Markets resolve times
[**sportMarketSelectionsGet**](MarketApi.md#sportMarketSelectionsGet) | **GET** /{sport}/market/selections | List All Selections
[**sportMarketStatesGet**](MarketApi.md#sportMarketStatesGet) | **GET** /{sport}/market/states | List All Markets states


<a name="sportMarketCloseTimesGet"></a>
# **sportMarketCloseTimesGet**
> sportMarketCloseTimesGet(sport)

List All Markets close times



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MarketApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMarketCloseTimesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportMarketResolveTimesGet"></a>
# **sportMarketResolveTimesGet**
> sportMarketResolveTimesGet(sport)

List All Markets resolve times



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MarketApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMarketResolveTimesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportMarketSelectionsGet"></a>
# **sportMarketSelectionsGet**
> sportMarketSelectionsGet(sport)

List All Selections



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MarketApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMarketSelectionsGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportMarketStatesGet"></a>
# **sportMarketStatesGet**
> sportMarketStatesGet(sport)

List All Markets states



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MarketApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMarketStatesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


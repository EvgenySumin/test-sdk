# PullApi.LiveApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportLiveMatchesGet**](LiveApi.md#sportLiveMatchesGet) | **GET** /{sport}/live/matches | List All Live Matches
[**sportLiveMatchesIdFeedGet**](LiveApi.md#sportLiveMatchesIdFeedGet) | **GET** /{sport}/live/matches/{id}/feed | Match feed
[**sportLiveMatchesIdStateGet**](LiveApi.md#sportLiveMatchesIdStateGet) | **GET** /{sport}/live/matches/{id}/state | Match state
[**sportLiveMatchesIdStatisticsFeedGet**](LiveApi.md#sportLiveMatchesIdStatisticsFeedGet) | **GET** /{sport}/live/matches/{id}/statistics/feed | Match statistics feed
[**sportLiveMatchesIdStatisticsStateGet**](LiveApi.md#sportLiveMatchesIdStatisticsStateGet) | **GET** /{sport}/live/matches/{id}/statistics/state | Match statistics state
[**sportLiveSeriesGet**](LiveApi.md#sportLiveSeriesGet) | **GET** /{sport}/live/series | List All Live Series
[**sportLiveSeriesIdFeedGet**](LiveApi.md#sportLiveSeriesIdFeedGet) | **GET** /{sport}/live/series/{id}/feed | Series feed
[**sportLiveSeriesIdStateGet**](LiveApi.md#sportLiveSeriesIdStateGet) | **GET** /{sport}/live/series/{id}/state | Series state


<a name="sportLiveMatchesGet"></a>
# **sportLiveMatchesGet**
> sportLiveMatchesGet(sport, opts)

List All Live Matches



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'leagueId': 3.4, // Number | ID of the league in the form of an integer
  'tournamentId': 3.4, // Number | ID of the tournament in the form of an integer
  'seriesId': 3.4 // Number | ID of the series in the form of an integer
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveMatchesGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **leagueId** | **Number**| ID of the league in the form of an integer | [optional] 
 **tournamentId** | **Number**| ID of the tournament in the form of an integer | [optional] 
 **seriesId** | **Number**| ID of the series in the form of an integer | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveMatchesIdFeedGet"></a>
# **sportLiveMatchesIdFeedGet**
> sportLiveMatchesIdFeedGet(id, sport, opts)

Match feed



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the match in the form of an integer

var sport = "sport_example"; // String | Sport key

var opts = { 
  'sequence': 3.4 // Number | Sequence number for filtering states
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveMatchesIdFeedGet(id, sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the match in the form of an integer | 
 **sport** | **String**| Sport key | 
 **sequence** | **Number**| Sequence number for filtering states | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveMatchesIdStateGet"></a>
# **sportLiveMatchesIdStateGet**
> sportLiveMatchesIdStateGet(id, sport, opts)

Match state



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the match in the form of an integer

var sport = "sport_example"; // String | Sport key

var opts = { 
  'sequence': 3.4, // Number | Sequence number for filtering
  'expand': "expand_example" // String | Expand match detail
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveMatchesIdStateGet(id, sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the match in the form of an integer | 
 **sport** | **String**| Sport key | 
 **sequence** | **Number**| Sequence number for filtering | [optional] 
 **expand** | **String**| Expand match detail | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveMatchesIdStatisticsFeedGet"></a>
# **sportLiveMatchesIdStatisticsFeedGet**
> sportLiveMatchesIdStatisticsFeedGet(id, opts)

Match statistics feed



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the match in the form of an integer

var opts = { 
  'sequence': 3.4 // Number | Sequence number for filtering states
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveMatchesIdStatisticsFeedGet(id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the match in the form of an integer | 
 **sequence** | **Number**| Sequence number for filtering states | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveMatchesIdStatisticsStateGet"></a>
# **sportLiveMatchesIdStatisticsStateGet**
> sportLiveMatchesIdStatisticsStateGet(id, opts)

Match statistics state



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the match in the form of an integer

var opts = { 
  'sequence': 3.4, // Number | Sequence number for filtering
  'expand': "expand_example" // String | Expand match detail
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveMatchesIdStatisticsStateGet(id, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the match in the form of an integer | 
 **sequence** | **Number**| Sequence number for filtering | [optional] 
 **expand** | **String**| Expand match detail | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveSeriesGet"></a>
# **sportLiveSeriesGet**
> sportLiveSeriesGet(sport, opts)

List All Live Series



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'leagueId': 3.4, // Number | ID of the league in the form of an integer
  'tournamentId': 3.4 // Number | ID of the tournament in the form of an integer
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveSeriesGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **leagueId** | **Number**| ID of the league in the form of an integer | [optional] 
 **tournamentId** | **Number**| ID of the tournament in the form of an integer | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveSeriesIdFeedGet"></a>
# **sportLiveSeriesIdFeedGet**
> sportLiveSeriesIdFeedGet(id, sport, opts)

Series feed



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the match in the form of an integer

var sport = "sport_example"; // String | Sport key

var opts = { 
  'sequence': 3.4 // Number | Sequence number for filtering states
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveSeriesIdFeedGet(id, sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the match in the form of an integer | 
 **sport** | **String**| Sport key | 
 **sequence** | **Number**| Sequence number for filtering states | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportLiveSeriesIdStateGet"></a>
# **sportLiveSeriesIdStateGet**
> sportLiveSeriesIdStateGet(id, sport, opts)

Series state



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LiveApi();

var id = 3.4; // Number | ID of the series in the form of an integer

var sport = "sport_example"; // String | Sport key

var opts = { 
  'sequence': 3.4, // Number | Sequence number for filtering
  'expand': "expand_example" // String | Expand series detail
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLiveSeriesIdStateGet(id, sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID of the series in the form of an integer | 
 **sport** | **String**| Sport key | 
 **sequence** | **Number**| Sequence number for filtering | [optional] 
 **expand** | **String**| Expand series detail | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


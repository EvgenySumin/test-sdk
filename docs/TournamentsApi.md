# PullApi.TournamentsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportTournamentsGet**](TournamentsApi.md#sportTournamentsGet) | **GET** /{sport}/tournaments | List All Tournaments


<a name="sportTournamentsGet"></a>
# **sportTournamentsGet**
> sportTournamentsGet(sport, opts)

List All Tournaments



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.TournamentsApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'dateFrom': "dateFrom_example", // String | Date to which filter tournaments
  'dateTo': "dateTo_example" // String | Date to which filter tournaments
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportTournamentsGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **dateFrom** | **String**| Date to which filter tournaments | [optional] 
 **dateTo** | **String**| Date to which filter tournaments | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


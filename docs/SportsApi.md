# PullApi.SportsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportsGet**](SportsApi.md#sportsGet) | **GET** /sports | List All Sports


<a name="sportsGet"></a>
# **sportsGet**
> sportsGet()

List All Sports



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.SportsApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportsGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


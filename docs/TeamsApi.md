# PullApi.TeamsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportTeamsGet**](TeamsApi.md#sportTeamsGet) | **GET** /{sport}/teams | List All Teams


<a name="sportTeamsGet"></a>
# **sportTeamsGet**
> sportTeamsGet(sport)

List All Teams



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.TeamsApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportTeamsGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


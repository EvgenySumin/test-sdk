# PullApi.PlayersApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportPlayersGet**](PlayersApi.md#sportPlayersGet) | **GET** /{sport}/players | List All Players


<a name="sportPlayersGet"></a>
# **sportPlayersGet**
> sportPlayersGet(sport, opts)

List All Players



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.PlayersApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'teamId': 3.4 // Number | Filter players by team id
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportPlayersGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **teamId** | **Number**| Filter players by team id | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


# PullApi.MatchesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportMatchesGet**](MatchesApi.md#sportMatchesGet) | **GET** /{sport}/matches | List All Matches in Series


<a name="sportMatchesGet"></a>
# **sportMatchesGet**
> sportMatchesGet(sport, opts)

List All Matches in Series



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MatchesApi();

var sport = "sport_example"; // String | Sport key

var opts = { 
  'tournamentId': 3.4, // Number | ID of the tournament in the form of an integer
  'leagueId': 3.4, // Number | ID of the league in the form of an integer
  'seriesId': 3.4 // Number | ID of the series in the form of an integer
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMatchesGet(sport, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 
 **tournamentId** | **Number**| ID of the tournament in the form of an integer | [optional] 
 **leagueId** | **Number**| ID of the league in the form of an integer | [optional] 
 **seriesId** | **Number**| ID of the series in the form of an integer | [optional] 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


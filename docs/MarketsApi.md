# PullApi.MarketsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportMarketsGet**](MarketsApi.md#sportMarketsGet) | **GET** /{sport}/markets | List All Markets


<a name="sportMarketsGet"></a>
# **sportMarketsGet**
> sportMarketsGet(sport)

List All Markets



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MarketsApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMarketsGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


# PullApi.LeaguesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportLeaguesGet**](LeaguesApi.md#sportLeaguesGet) | **GET** /{sport}/leagues | List All Leagues


<a name="sportLeaguesGet"></a>
# **sportLeaguesGet**
> sportLeaguesGet(sport)

List All Leagues



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.LeaguesApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportLeaguesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8


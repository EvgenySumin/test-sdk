# PullApi.MatchApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sportMatchPositionsGet**](MatchApi.md#sportMatchPositionsGet) | **GET** /{sport}/match/positions | List All Match positions
[**sportMatchStatesGet**](MatchApi.md#sportMatchStatesGet) | **GET** /{sport}/match/states | List All Matches states


<a name="sportMatchPositionsGet"></a>
# **sportMatchPositionsGet**
> sportMatchPositionsGet(sport)

List All Match positions



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MatchApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMatchPositionsGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

<a name="sportMatchStatesGet"></a>
# **sportMatchStatesGet**
> sportMatchStatesGet(sport)

List All Matches states



### Example
```javascript
var PullApi = require('pull_api');

var apiInstance = new PullApi.MatchApi();

var sport = "sport_example"; // String | Sport key


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.sportMatchStatesGet(sport, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sport** | **String**| Sport key | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json; charset=utf-8

